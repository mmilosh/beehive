### Steps to Form Proper Documentation for Component[^1] Software Development

- create project
- create Git project structure
- define modules[^4] 
- define features[^5]
- define user stories[^6]
- divide user stories into epics
- define roles[^8] and authorization policies
- define and document APIs (Swagger)
- define DTO model
- define and document interfaces
- define dependencies[^9] (libraries)
- define data sources[^2] (infrastructure)
- define test cases for user stories[^7]
- create DTO classes
- create DTO validators
- create DTO serializers
- create mocks/stubs for data sources[^2] and dependency[^9] interfaces
- create unit tests per component[^1]
- define request/message controllers[^3] (error handling, DTO validation, serialization, message queue communication)
- create component[^1]
- create Dockerfile
- validate component[^1] thru CI

[^1]: Component - class or library of classes implementing business logic of a user story
[^2]: Data source - internal implementation wrapping real data source communication logic (db connection, document repository, microservice API call, etc)
[^3]: Controller - boilerplate implementation used to invoke Service component[^1]. Controller logic focuses on error handling, DTO validation, serialization, message queue communication and etc
[^4]: Module - (user management, projects, vendor management...)
[^5]: Feature - 
[^6]: User Story - 
[^7]: User Story Test Case - 
[^8]: Role
[^9]: Dependency

---

### Roles (TODO)
- admin
- project admin (PA)
- production manager (PM)
...

---

### Modules
- Document Repository (UML, BPML, Use Cases, Services, Modules, Classes, Interfaces, Code Convention, Project Docs, Code Folder Structure, Test Classes, UI, UX, Themes, DB Schema, Models, Feature) 
- Database (Organisation, Project, Service, Module, Class, Allowed Vendors, Teams, Jobs, Tasks, PM Roles, Vendor Roles, Source Code Repository, Phase) 
- Vendor Management (Name, CV, Score, Reviews, Projects, Type (coder, QA, UI, UX, BPML), Invoice info) 
- WorkFlow Engine (Tasks, Jobs, Job Templates) 
- PM Management (Name, Type, Reviews) 

---

### Features
- Multi-tenant 
- Multi Cluster 
- WorkFlow Engine 
- Document Repository 
- Vendor Management 
- PM Management 
- Interface Manager 
- Class Manager 
- Roles (PM, docwriter , coder, tester, codeqm, codeproofer) 
- Classes (DB, Service, API, UI, Dao, Dto) 
- Identity Federation (Social , Internal) 
- Budget 
- Vendor Invoice 
- System Roles (admin, org user) 
- Client Portal 
- Vendor Portal 
- Notifications 

---

### Project Structure

```bash
Project
 ↳ Epic 1..*
  ↳ Feature 1..*
   ↳ User Story 1..*
    ↳ Acceptance Criteria 1..*

Project
 ↳ Epic 1..*
  ↳ Module 1..*
   ↳ Feature 1..*
    ↳ Gherkin Story 1..*
     ↳ Gherkin Test 1..*

Project
 ↳ Module 1..*
  ↳ Service 1..*
   ↳ Library 1..*
    ↳ Class 1..*

Project
 ↳ Job 1..*
  ↳ Phase 1..*
   ↳ Task 1..*
    ↳ Library 1..*
    ↳ Class 1..*
``` 

Each user story with acceptance criteria is defined using Gherkin format (GWT).

---