```mermaid
erDiagram
    PROJECT {
        Id Id
        string Name
        string Summary
        ComponentType[] Available         

        blob SRS
        PM[] AssignedPMs
        Vendor[] ApprovedVendors
        Team[] Teams
    }
    MODULE {
        Id Id
        Id ProjectId
        string Name
        string Summary 
        string[] Roles
        ComponentType[] Default         
    }
    FEATURE {
        Id Id
        Id ModuleId
        string Name
        string Summary 
        string[] Roles         
        ComponentType[] Default         
    }
    USERSTORY {
        Id Id
        Id FeatureId
        string Epic
        GherkinScenario Scenario
        GherkinTestCase[] TestCases
    }
    JOB {
        Id Id
        Id ProjectId
        UserStory[] UserStories
    }
    PHASE {
        Id Id
        Id JobId
        ComponentType ComponentType
        DateTime Deadline 
    }
    TASK {
        Id Id
        Id PhaseId
        string Name
    }
    COMPONENTTYPE {
        Id Id
        Id[] ComponentTypeDependencies 
    }
    COMPONENT {
        Id ComponentTypeId
    }
    USERSTORYCOMPONENT {
        Id Id
        Id UserStoryId
        Id ComponentId
    }
```

 * ModuleType (DB, Repository, UI)
 * ServiceType (DB, Doc Repository, Business Logic, UI) 
 * Dependencies (DB, Doc Repository) 
 * ClassType (Main, Integration Test, Load Test, Performance Test) 
 * VendorType (UX, UI, coder, QA, QM)
 * JobType (UML, BPML, Use Cases, Project Docs, Classes, Services, Modules, UI, UX, Theme)
 * TaskType (Manual, Automation) 
 * PolicyType (Read/Write ?) 