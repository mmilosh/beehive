## Common Roles
|| Read | Write | Comment |
|:---|:---:|:---:|:---:|   
| _**Org Admin**_ | 
| Organisation          | x | x |   | 
| Org Members           | x | x |   | 

## Product Owner (PO) Portal
|| Read | Write | Comment |
|:---|:---:|:---:|:---:|   
| _**Product Admin**_ | 
| Org Members           | x |   |   | 
| Team                  | x | x |   | 
| Product               | x | x | x | 
|| 
| _**Product Owner**_ | 
| Product               | x | x | x | 
| Module                | x | x | x | 
| Feature               | x | x | x | 
| User Story            | x | x | x | 
| Budget Approval       | x | x | x | 
| Sign-off              | x | x | x | 
| Assign Member         | x | x |   | 
|| 
| _**Module Owner**_ | 
| Product               | x |   | x | 
| Module                | x | x | x | 
| Feature               | x | x | x | 
| User Story            | x | x | x | 
| Budget Approval       | x | x | x | 
| Sign-off              | x | x | x | 
| Assign Member         | x | x |   | 
|| 
| _**Feature Owner**_ | 
| Product               | x |   | x | 
| Module                | x |   | x | 
| Feature               | x | x | x | 
| User Story            | x | x | x | 
| Budget Approval       | x | x | x | 
| Sign-off              | x | x | x | 
| Assign Member         | x | x |   | 
|| 
| _**Actor**_ | 
| Product               | x |   |   | 
| Module                | x |   |   | 
| Feature               | x |   | x | 
| User Story            | x | x | x | 
| Assign Member         | x |   |   | 

## Project Manager (PM) Portal
|| Read | Write | Comment |
|:---|:---:|:---:|:---:|   
| _**Project Admin**_ | 
| Org Members           | x |   |   | 
| Team                  | x | x |   | 
| Project               | x | x | x | 
|| 
| _**Project Manager**_ | 
| Project               | x | x | x | 
| User Story            | x |   | x | 
| Component             | x |   | x | 
| Epic                  | x | x |   | 
| Milestone             | x | x |   | 
| Jobs                  | x | x | x | 
| Phase                 | x | x | x | 
| Budget Approval       | x | x | x | 
| Profit                | x | x |   | 
| Invoice Approval      | x | x | x | 
| Sign-off              | x | x |   | 
| Approve Vendors       | x | x |   | 
| Vendors Teams         | x | x |   | 
| Assign Member         | x | x |   | 
|| 
| _**Epic Manager**_ | 
| Project               | x |   | x | 
| User Story            | x |   | x | 
| Component             | x |   | x | 
| Epic                  | x | x |   | 
| Milestone             | x | x |   | 
| Jobs                  | x | x | x | 
| Phase                 | x | x | x | 
| Budget Approval       | x | x | x | 
| Profit                | x | x |   | 
| Invoice Approval      | x | x | x | 
| Sign-off              | x | x |   | 
| Approve Vendors       | x | x |   | 
| Vendors Teams         | x | x |   | 
| Assign Member         | x | x |   | 
|| 
| _**Milestone Manager**_ | 
| Project               | x |   | x | 
| User Story            | x |   | x | 
| Component             | x |   | x | 
| Epic                  | x |   |   | 
| Milestone             | x | x |   | 
| Jobs                  | x | x | x | 
| Phase                 | x | x | x | 
| Budget Approval       | x | x | x | 
| Profit                | x | x |   | 
| Invoice Approval      | x | x | x | 
| Sign-off              | x | x |   | 
| Approve Vendors       | x | x |   | 
| Vendors Teams         | x | x |   | 
| Assign Member         | x | x |   | 
|| 
| _**Actor**_ | 
| Project               | x |   | x | 
| User Story            | x |   | x | 
| Component             | x |   | x | 
| Epic                  | x |   |   | 
| Milestone             | x |   |   | 
| Jobs                  | x |   | x | 
| Phase                 | x |   | x | 
| Budget Approval       | x |   | x | 
| Profit                | x |   |   | 
| Invoice Approval      | x | x | x | 
| Approve Vendors       | x |   |   | 
| Vendors Teams         | x |   |   | 
| Assign Member         | x |   |   | 

## Vendor Portal
|| Read | Write | Comment |
|:---|:---:|:---:|:---:|   
| _**Project Admin**_ | 
| Org Members           | x |   |   | 
| Team                  | x | x |   | 
| Project               | x | x | x | 
|| 
| _**Project Manager**_ | 
| Project               | x | x | x | 
| User Story            | x |   | x | 
| Component             | x | x | x | 
| Proposal              | x | x | x | 
| Invoice               | x | x | x | 
| Assign Member         | x | x |   | 
|| 
| _**Actor**_ | 
| Project               | x |   | x | 
| User Story            | x |   | x | 
| Component             | x | x | x | 
