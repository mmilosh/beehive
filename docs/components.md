```mermaid
flowchart TB
    A["User Story (Feature)"] ---> B[Swagger]
    A ---> C["BPMN Diagram"]
    A ---> D["User Story Test Cases"]
    I["UI Unit Tests"] ---> E["UI Service"]
    G["UI Interface"] ---> I
    F[UX] ---> H["UI DTO Model"]
    H ---> G
    D ---> I
    A ---> F
    B ---> J["Backend DTO Model"]
    J ---> K["Backend Interface"]
    K ---> L["Backend Unit Tests"]
    D ---> L
    L ---> M["Backend Service"]
    B ---> N["Entity Relationship (ER) Diagram"]
    K ---> N
    N ---> O["DB Schema"]
    B ---> P["Repository Structure"]
    K ---> P
    C ---> Q["Workflow Definition"]
```

## Component Types and Dependency Tree

### Common Components
- **User Story (Feature)** - _basic unit describing simple system action or set of related units (feature)_
- **Swagger** [^1] - _interface description containing method and parameter definitions, validation rules, input and output value definitions_
    - User Story (Feature)
- **BPMN Diagram** - important business process definition diagram_ 
    - User Story (Feature)
- **User Story Test Cases** - _acceptance criteria for user story_
    - User Story (Feature)

### User Interface (UI) Components
- **UX** - _wireframe UI design_
    - User Story (Feature)
    - _Swagger ???_
- **UI Design** - _final UI design delivered as set of images_
    - User Story (Feature)
    - UX
- **UI CSS** - _final CSS component based on UI design_
    - User Story (Feature)
    - UX
    - UI Design
- **UI DTO Model** - _DTO model for UI component_
    - User Story (Feature)
    - UX
    - Swagger
- **UI Interface** - _interface for UI component_
    - User Story (Feature)
    - UX
    - UI DTO Model
    - _Swagger ???_
- **UI Unit Tests** - _test cases for UI Service implementation validation_
    - User Story (Feature)
    - User Story Test Cases
    - UI DTO Model
    - UI Interface
    - _UX ???_
- **UI Service** - _UI Service implementation_
    - User Story (Feature)
    - UX
    - UI Interface
    - UI DTO Model
    - UI Unit Tests

### Backend Components
- **Backend DTO Model** - _DTO model for backend component_
    - User Story (Feature)
    - Swagger
- **Backend Interface** - _interface for backend component_
    - User Story (Feature)
    - Backend DTO Model
    - Swagger
- **Backend Unit Tests** - _test cases for backend Service implementation validation_
    - User Story (Feature)
    - User Story Test Cases
    - Backend DTO Model
    - Backend Interface
- **Backend Service** - backend Service implementation_
    - User Story (Feature)
    - Backend Interface
    - Backend DTO Model
    - Backend Unit Tests

### Repository Components
- **Entity Relationship (ER) Diagram** 
    - User Story (Feature)
    - _Swagger ???_
    - Backend Interface
    - Backend DTO Model
- **DB Schema**
    - User Story (Feature)
    - ER Diagram
    - _Swagger ???_
    - Backend Interface
    - Backend DTO Model
- **Data Repository Structure**
    - User Story (Feature)
    - _Swagger ???_
    - Backend Interface
    - Backend DTO Model

### Workflow Components
- **Workflow Definition**
    - User Story (Feature)
    - BPMN Diagram

[^1]: TODO - Better name