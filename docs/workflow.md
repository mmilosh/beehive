## Project Workflow

### Project
Product Manager creates new project. Project contains modules and features. Epics are also part of a project for scheduling purposes. Project is created based on a project template containing the list of available component types (UX, UI, UML, BPMN, Test, Service and etc). Project configuration defines if the project is budget-based or based on first come first served principle. 
[Project BPMN](./bpmn/project.bpmn)

### User Stories
Product Manager adds user stories to a project. Each user story is tagged with a feature and an epic. Each feature is tagged with a module. Modules can contain subset of available component types and define default set of component types for user stories.
User Story contains scenario and acceptance criteria in Gherkin format. User story also contains set of component types required for the full user story implementation. 
_TODO: example user story with component types_

### Components
Each component is defined by its type. There is a dependency between component types (e.g. UI Design component depends on UX component, Service component depends on Unit test component among others and etc).

### Workflow Jobs
Production Manager creates a job by selecting one or more user stories (feature) to be processed by the job. Based on the selection job phases are created for each individual component type belonging to each selected user story/feature. If a project is budget-based, first production manager negotiates budget with vendors. Otherwise, phase processing starts. For budget-based project once budget is approved accepted vendor is assigned to a phase, phase becomes reserved and phase processing starts.
[Jobs BPMN](./bpmn/jobs.bpmn)

### Phase Processing
Phase processing starts with the phase being listed for reservation. This step is skipped for budget-based projects since the phase has been already reserved. Phase processing is placed on hold while all dependencies are finalized. Phase is defined by component type. Since there are dependencies between different component types hence the dependencies between phases. 

Before listing phase for claiming by vendors workflow verifies if phase has been reserved. Phase can get reserved while dependencies are getting finalized.

Once phase gets assigned or claimed and all dependencies are finalized workflow continues processing the phase. Phase contains detailed log of all events and date/time stamps. In case of phase processing failure or time expiration, production manager is able to make proper phase analysis, continue with existing vendor to complete phase or assigned different vendor to assure the completion. There is a link between phases and jobs for time and budget reporting purposes.
[Tasks BPMN](./bpmn/tasks.bpmn)


### Tasks
Workflow offers series of manual tasks enabling completion of a phase. The following tasks are available:
- claim
- PM assign
- auto-assign (round-robin)
- download
- process
- upload
- release
- reject
- PM release
- budget proposal
- negotiate budget
- approve budget